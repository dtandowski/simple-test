package com.tinasuper.dev.simpletest

import org.springframework.http.HttpStatus.*
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.server.ResponseStatusException

@Controller
class HTMLController(private val repository: ArticleRepository) {

    @GetMapping("/")
    fun front(model: Model): String {
	model["title"] = "simpletest"
	model["articles"] = repository.findAllByOrderByAddedAtDesc().map {
	    it.render()
	}
	return "front"
    }

    @GetMapping("/article/{slug}")
    fun article(@PathVariable slug: String, model: Model): String {
	val article = repository.findBySlug(slug)?.render() ?: throw ResponseStatusException(
		NOT_FOUND,
		"This article does not exist")
	model["title"] = article.title
	model["article"] = article
	return "article"
	
    }

    fun Article.render() = RenderedArticle(
	slug,
	title,
	headline,
	content,
	author,
	addedAt.format())

    data class RenderedArticle(
	val slug: String,
	val title: String,
	val headline: String,
	val content: String,
	val author: User,
	val addedAt: String)

}
