package com.tinasuper.dev.simpletest

import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SimpleTestConfiguration {

    @Bean
    fun databaseInitializer(userRepository: UserRepository,
			    articleRepository: ArticleRepository) =
	ApplicationRunner {
	    val dtando = userRepository.save(User("dtando", "Daniel", "Tandowski"))
	    articleRepository.save(Article(
				       title = "Some title",
				       headline = "Yo headline me!",
				       content = "much content, wow",
				       author = dtando))
	    articleRepository.save(Article(
				       title = "Another article",
				       headline = "Check out this article",
				       content = "Not very interesting content",
				       author = dtando))
}
}
