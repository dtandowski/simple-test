package com.tinasuper.dev.simpletest

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SimpleTestApplication

fun main(args: Array<String>) {
    runApplication<SimpleTestApplication>(*args)
//	setBannerMode(Banner.Mode.OFF)
}

