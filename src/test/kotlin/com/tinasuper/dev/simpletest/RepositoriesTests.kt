package com.tinasuper.dev.simpletest

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class RepositoriesTests @Autowired constructor(
    val entityManager: TestEntityManager,
    val userRepository: UserRepository,
    val articleRepository: ArticleRepository) {

    @Test
    fun `When findByIdOrNull then return Article`() {
	val juergen = User("jklopp", "Juergen", "Klopp")
	entityManager.persist(juergen)

	val kloppart = Article("Klopp is the best",
				 "The legend of Juergen Klopp",
				 "So here's the thing about Klopp...",
				 juergen)
	entityManager.persist(kloppart)
	entityManager.flush()

	val found = articleRepository.findByIdOrNull(kloppart.id!!)
	assertThat(found).isEqualTo(kloppart)
    }

    @Test
    fun `When findByLogin then return User`() {
	val juergen = User("jklopp", "Juergen", "Klopp")
	entityManager.persist(juergen)
	entityManager.flush()
	val user = userRepository.findByLogin(juergen.login)
	assertThat(user).isEqualTo(juergen)
	
    }
}
